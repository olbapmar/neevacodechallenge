# ACME Mobile Browser (Android) | Neeva Code Challenge

### By: Pablo Pastor Martín

This is my submission to the code challenge I was requested to do for being considered for a position at Neeva. 

Although my professional background **is not** mobile development, I decided to give it a try for being both challenging and an opportunity lo learn new stuff. But please, keep in mind that this is the first mobile application I've developed.

## The challenge

The app to deliver is a  basic web browser UI that supports navigating the web on a mobile device and built on top of Android’s built-in WebView component. It has to provide a URL entry field, back, forward and reload buttons as well as a way to switch tabs.

Additionally, as the user browses around the web, the URL entry field should be updated to reflect the URL of the currently loaded web page.

On top of it, an additional extra feature must also be implemented, as we'll discuss later.

## How to build and run

In order to build and run the project, simply import the project in Android Studio and press the run button on any Android Virtual Device that you might have or proceed to create one if necessary. **Note**: Please keep inmind that `minSdk` is 21.

## Results

The result of project meets the requirements of the initial description and implements one of the proposed extra features as well as some other minor features. 

First, the app has a "status bar" with the URL entry field and buttons for going back, forward and reload the current page. Lastly, this bar also contains a button that opens a dialog for tab managing. 

![Default tab and URL](readme-resources/screenshot1.png)

The tab dialog displays the thumbnails of the opened tabs to facilitate navigation through tabs to users, being this the extra feature implemented. This dialog contains a button for creating a new tab in the upper right corner. 

Each tab 'item' contains the thumbnail, favicon and title of the page displayed on the tab, being the thumbnail and title clickable to switch to the selected tab. On the right-hand side of the title a little cross icon is displayed, which will close the tab when clicked.

![Tabs dialog](readme-resources/screenshot2.png)

If the closed tab is the only tab opened, a new tab is automatically created and directed to the default url, which is (of course ;) ) [neeva.com](neeva.com)

The icons used for the UI are obtained from [Icons8](https://icons8.com) and do not belong to me. 

As extra 'little' features, I want to highlight: 

- If the url introduces by the user does not start with http or https, https is automatically added.
- A progress bar displays the loading page process so that the user can know how the loading process is being developed
- 'Back' button support
- The current tab has its title in bold text

### Hard decisions

The most 'challenging' decision I had to make was related to when to obtain the thumbnails of the tabs, that's because webview does not support drawing to a bitmap if the page is not being displayed and also because there is no method that can be overriden to know when the page is 
fully loaded, making that the majority of times there the thumbnail is generated in `onPageFinished`, this thumbnail does not reflect the actual look and feel of the page. 

On the other hand, constantly generating a thumbnail would be tremendously expensive in computational resources, so an intermediate solution must be found.

The approach I came up with was generating thumbnails on `onPageFinished` and a new thumbnail for the active tab for whenever the uses presses the 'tabs' icon, that way we assure that the last
appearance the user saw from a page matches the thumbnail the user sees on the tabs dialog without overloading the CPU. 

## Further improvements

This section contains improvements I would do I had had more time:

- Improve the look and feel of the app by adding animations on user actions and supporting gestures (swipe up to reload, etc)
- Allow users to customize the default URL via config menu
- Other proposed features such as URLS suggestions
- Default search engine for whenever the users introduces a string that is not a url
- Tests  (I would have loved to make tests, but I hadn't time to learn about them :( )